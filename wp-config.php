<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'emarket');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pL>#rwRp}~&KR.]()<c zAQ(FxepLG~?TjdtF00i2bpT_U{mhxT^q$xJo=7~@R:B');
define('SECURE_AUTH_KEY',  '6JWV6O+$y@mR)cQ>vaRUpmc^*~I{Qr|8M6EXrNM8#xO+E] l4_*cWb1husK1u(Gu');
define('LOGGED_IN_KEY',    '>RX}P4zv%svG`0mKz^!7d*1^kmrQJ:TK}/Kxf*_v6du]_0RT<l8y)js|ZCJM&G*[');
define('NONCE_KEY',        'oFKcn?|H~`3z5zt*;it?~344I<$3p9gjis([^C{Y|6{.W ],%q3))vZ 0B79?RP=');
define('AUTH_SALT',        'BIoj=G1;ck/Sua|bXW?;7tSTBa7C|9LXLDG6?bV`Zd:7jB|Ty4c!DpLQ)D5c,7R[');
define('SECURE_AUTH_SALT', 'bLn{ssUUCV3.,m;>tVQ6G.H.EP;hbI}]xv&`tHA/Kj0`G/:n8tzGMsv+ObIpI {U');
define('LOGGED_IN_SALT',   '3MYbZgN]bUN*N-uba#44NVT(Hr_d`3)Pf(icvu/TAZyTa#r&<` ,P7)BS5l3HvU7');
define('NONCE_SALT',       ' ;O[8`u-Kos1:w63_E9=x>k3@Kvh(JVq~??/LwJ}a^.}OySED:Q}rBU+YV1GrS5,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
